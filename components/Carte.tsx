import { capitalize, convertToId, formatPrice } from '../helpers/utils'
import { MenuCarte } from '../models/MenuCarte'
import Article from './Article'

export interface CartePros {
    carte: MenuCarte
}

const Carte: React.FC<CartePros> = ({ carte }) => {
    return (
        <div className='pb-12'>
            <h1 id={convertToId(carte.name)} className='xl:w-2/3 mx-auto text-3xl bg-tea-blue text-center mb-3 py-6'>
                {carte.name}
            </h1>

            {carte.category.sort((a, b) => a.order - b.order).map((cat, idx) => (
                <div className='xl:w-1/3 w-2/3 mx-auto px-3' key={idx}>
                    <h2 className='text-3xl mt-6'>
                        {cat.name}
                    </h2>

                    {cat.subCat.sort((a, b) => a.order - b.order).map((sb, idx) => (
                        <div key={idx}>
                            {sb.name.toLowerCase() !== "null" && (<div className='flex place-content-between  mt-3'>
                                <h3 className='text-xl underline'>{sb.name}</h3>
                                {sb.price && <p>{formatPrice(sb.price)}</p>}
                            </div>)}
                            {sb.description && (<p className='italic text-sm mt-1 text-justify'>
                                {sb.description}
                            </p>)}

                            <div>
                                {sb.articles.sort((a, b) => a.order - b.order).map((a, idx) => (
                                    <Article key={idx} article={a} />
                                ))}
                            </div>
                        </div>
                    ))}
                </div>
            ))}
        </div>
    )
}

export default Carte
import Image from 'next/image'
import { useState } from 'react'
import Link from 'next/link'

const Navbar = () => {
    const [isNavOpen, setIsNavOpen] = useState(false); // initiate isNavOpen state with false

    const menuLink = (isNav: boolean) => {
        return (
            <>
                <li className={isNav ? "border-b border-gray-400 my-8 uppercase" : ""}>
                    <Link href="/">A Propos</Link>
                </li>
                <li className={isNav ? "border-b border-gray-400 my-8 uppercase" : ""}>
                    <Link href="/menu">Notre Carte</Link>
                </li>
                <li className={isNav ? "border-b border-gray-400 my-8 uppercase" : ""}>
                    <Link href="/#contact">Contact</Link>
                </li>
            </>
        )
    }

    return (
        <div className="flex items-center justify-between border-b border-gray-400 py-8 bg-tea-pink fixed w-full z-50">
            <nav className="ml-8">
                <section className="MOBILE-MENU flex lg:hidden">
                    <div
                        className="HAMBURGER-ICON space-y-2"
                        onClick={() => setIsNavOpen((prev) => !prev)} // toggle isNavOpen state on click
                    >
                        <span className="block h-0.5 w-8 animate-pulse bg-gray-600"></span>
                        <span className="block h-0.5 w-8 animate-pulse bg-gray-600"></span>
                        <span className="block h-0.5 w-8 animate-pulse bg-gray-600"></span>
                    </div>

                    <div className={isNavOpen ? "showMenuNav bg-tea-pink" : "hideMenuNav"}>
                        <div
                            className="CROSS-ICON absolute top-0 left-0 px-8 py-8"
                            onClick={() => setIsNavOpen(false)}
                        >
                            <svg
                                className="h-8 w-8 text-gray-600"
                                viewBox="0 0 24 24"
                                fill="none"
                                stroke="currentColor"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                            >
                                <line x1="18" y1="6" x2="6" y2="18" />
                                <line x1="6" y1="6" x2="18" y2="18" />
                            </svg>
                        </div>
                        <ul className="MENU-LINK-MOBILE-OPEN flex flex-col items-center justify-between min-h-[250px]">
                            {menuLink(true)}
                        </ul>
                    </div>
                </section>

                <ul className="DESKTOP-MENU hidden space-x-8 lg:flex">
                    {menuLink(false)}
                </ul>
            </nav>
            <style>{`
      .hideMenuNav {
        display: none;
      }
      .showMenuNav {
        display: block;
        position: absolute;
        width: 100%;
        height: 100vh;
        top: 0;
        left: 0;
        z-index: 10;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: center;
      }
    `}</style>
        </div>
    );
}

export default Navbar
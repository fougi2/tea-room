import Head from 'next/head'
import Navbar from "./Navbar"
import Footer from "./Footer"

interface LayoutProps {
    children?: React.ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
    return <>
        <Head>
            <title>Le Jardin des Thés</title>
            <meta name="description" content="Le Jardin des Thés" />
            <link rel="icon" href="/favicon.png" />
        </Head>
        <Navbar />
        <div className='pt-20'>{children}</div>
        {/* <Footer /> */}
    </>
}

export default Layout
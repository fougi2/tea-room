import { capitalize, formatPrice } from '../helpers/utils'
import { Article } from '../models/Article'

export interface CartePros {
    article: Article
}

const Article: React.FC<CartePros> = ({ article }) => {

    return (
        <>
            <div className='flex place-content-between mt-3 items-center'>
                <h4 className='text-lg'>{article.name}</h4>
                {article.price && <p>{formatPrice(article.price)}</p>}
            </div>
            {
                article.description
                && (
                    <p className='italic text-sm text-justify'>
                        {article.description}
                    </p>
                )
            }
        </>
    )
}

export default Article
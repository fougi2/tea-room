import Image from 'next/image'
import { useState } from 'react'

const Footer = () => {
    return (
        <footer className="footer">
            <div className="content has-text-centered">
                <p>
                    <strong>Le Jardin des Thés</strong> +41 79 752 21 47
                </p>
                <p>
                    Le Bruet 1867 St-Triphon.
                </p>
            </div>
        </footer>
    )
}

export default Footer
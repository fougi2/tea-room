import type { NextPage } from 'next'
import Image from 'next/image'
import Layout from '../components/Layout'
import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Carte } from '../models/Carte';
import { convertToId } from '../helpers/utils';
import { Infos } from '../models/Infos';
import { info } from 'console';

interface HomeProps {
  carte: Carte[],
  infos: Infos
}

const Home = ({ carte, infos }: HomeProps) => {
  const router = useRouter()
  const settings = {
    variableWidth: true,
    infinite: true,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 2000,
  };
  return (
    <Layout>
      <main className={`${styles.descriptions} mx-auto text-center`}>
        <div className='py-20 bg-center bg-hero-pattern bg-no-repeat bg-cover '>
          <div className='bg-white/60'>
            <img className='max-h-96  mx-auto' src="/le-jardin-des-thes.png" alt="Le Jardin des Thés" />
          </div>
        </div>
        {infos.isEventHappening && (<div className="py-12 bg-tea-blue">
          <h1 className='text-3xl mb-8'>En ce moment</h1>
          {infos.evenement?.map((s, idx) => (<p key={idx} className="2xl:w-1/3 w-2/3 mx-auto text-2xl">{s}</p>))}

        </div>)}
        <div className='py-12 bg-tea-pink'>
          <h1 className='text-3xl mb-8'>
            À propos du salon de thé
          </h1>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Le jardin des Thés « English Tearoom – Bar – Snack – Boutique », anciennement Venus et Rose English Tearoom a ouvert ses portes le 10 juin 2017. Sa fondatrice a voulu importer une tradition de sa terre natale l’Afrique du Sud où il y est coutume d'avoir un tearoom au sein des jardineries.
          </p>
          <p className="2xl:w-1/3 w-2/3 mx-auto"> C'est ainsi qu'elle a eu l'idée de créer son propre salon de thé anglais au sein de La Jardinerie à St-Triphon. </p>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Eric, le nouvel hôte, vous y reçoit avec plaisir et sourire, depuis le 17 mai 2022, du mardi au samedi, dans une atmosphère conviviale.
          </p>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Disposant de canapés confortables, le salon vous invite à vous prélasser le temps d'une tasse de thé ou durant une pause déjeuner pour déguster nos délicieux scones, tartes et autres mets.
            Situé dans l'espace vert de la jardinerie entouré par des fleurs, le snack-tearoom dispose d'une terrasse ombragée où il fait bon s’installer dans ses alcôves de verdures en ayant une vue imprenable sur les montagnes (Dents-du-Midi et les Dents de Morcles).
          </p>
        </div>
        <div className="py-12 bg-white">
          <h1 className='text-3xl mb-8'>La philosophie</h1>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Au Jardin des Thés « English Tearoom – Bar – Snack – Boutique », devenez nostalgique, notre porcelaine vous emmènera dans le passé.
          </p>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Nous vous accueillons chaleureusement dans une ambiance vintage.
            Toute notre vaisselle, a été récupérée auprès de particuliers, à la déchetterie, aux marchés aux puces, etc.
          </p>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Si vous avez de vieux pots, tasses, assiettes en fine porcelaine dont vous ne vous servez plus, n'hésitez pas à les apporter au tearoom.
          </p>
        </div>
        <div className="py-12 bg-tea-blue">
          <h1 className='text-3xl mb-8'>Nos gâteaux et délices anglais</h1>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Nous proposons une carte dont les gâteaux et pâtisseries changent souvent, Gâteaux, typiquement anglais, au café et Noix, aux carottes, aux chocolat décadent ou gâteau sans gluten, etc.., tous fait sur place.
          </p>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Ainsi que différents Scones : natures, aux fruits, aux pépites de chocolat et orange confite, mais aussi salés au Cheddar, tomates confites, selon l’humeur d’Éric, servis avec Jam et Crème, les sandwiches aux concombres ou aux œufs. D’autres plats sont disponibles tout au long de la journée.
          </p>
          <p className="2xl:w-1/3 w-2/3 mx-auto">
            Venez déguster une pie de Legends Pies, faite maison à Laax, une salade ou tout autre plat du jour proposé.
            Au niveau des boissons, nous offrons un large choix de thés anglais Tea People, ainsi que divers autres boissons, bières, vins et alcools.
          </p>
        </div>
        <div className="py-12 bg-white">
          <h1 className='text-3xl mb-8'>Boutique</h1>
          <p className="2xl:w-1/3 w-2/3 mx-auto">Nous vous proposons un choix d’articles d’épicerie anglaise, de thés Tea-People, de cadeaux divers.</p>
        </div>
        <div className={`px-8 mx-auto`} >
          <Slider {...settings}>
            <div><img className='h-80 mx-1' src="/carousel/blueberry_scones.jpg" alt="" /></div>
            <div><img className='h-80 mx-1' src="/carousel/scones.JPG" alt="" /></div>
            <div><img className='h-80 mx-1' src="/carousel/cakes1.jpg" alt="" /></div>
            <div><img className='h-80 mx-1' src="/carousel/cakes2.jpg" alt="" /></div>
            <div><img className='h-80 mx-1' src="/carousel/counter.jpg" alt="" /></div>
            <div><img className='h-80 mx-1' src="/carousel/jams.JPG" alt="" /></div>
          </Slider>
        </div>
        <div className="py-12 bg-white">
          <h1 className='text-3xl mb-8'>Notre Carte</h1>
          {
            carte.sort((a, b) => a.order - b.order).map((c: Carte, idx: number) => (
              <div key={`carte-${idx}`} className={`text-2xl mx-auto 2xl:w-1/3 w-2/3 py-8 ${idx % 2 == 0 ? "bg-tea-blue" : "bg-white"}`}>
                <a href={`/menu#${convertToId(c.name)}`}>
                  {c.name}
                </a>
              </div>
            ))
          }
          <p className='2xl:w-1/3 w-2/3 mx-auto underline italic mt-6'>Allergies - Intolérances</p>
          <p className='2xl:w-1/3 w-2/3 mx-auto italic'>Sur demande, nos collaborateurs vous renseigneront volontiers sur les ingrédients présents dans nos plats qui sont susceptibles de provoquer des allergies ou des intolérances</p>
          <p className='2xl:w-1/3 w-2/3 mx-auto underline italic mt-6'>Provenance de nos viandes</p>
          <p className='2xl:w-1/3 w-2/3 mx-auto italic'>Bovines : CH/IR/FR</p>
          <p className='2xl:w-1/3 w-2/3 mx-auto italic'>Ovines : IR/FR/NZ</p>
          <p className='2xl:w-1/3 w-2/3 mx-auto italic'>Poulets et porc : CH</p>
        </div>
        <div id='contact' className="py-12 bg-tea-pink">
          <h1 className='text-3xl mb-8'>Contact et Horaires</h1>
          {infos.contact.map((s, idx) => (<p key={idx} className="2xl:w-1/3 w-2/3 mx-auto text-xl">{s}</p>))}
          <p className='underline italic mt-6 text-2xl'>Nous sommes ouverts</p>
          {infos.horaires.map((s, idx) => (<p key={idx} className="2xl:w-1/3 w-2/3 mx-auto text-xl">{s}</p>))}
        </div>
      </main>
    </Layout>
  )
}

export async function getServerSideProps() {
  const [carteJson, infosJson] = await Promise.all([
    fetch(`${process.env.DIRECTUS_URL}/items/Carte`),
    fetch(`${process.env.DIRECTUS_URL}/items/Infos`),
  ])

  const [carteData, infosData] = await Promise.all([carteJson.json(), infosJson.json()])

  const carte: Carte[] = carteData.data.map((c: any) => ({
    id: c.id,
    name: c.Name,
    order: c.Order
  }))

  const infos: Infos = {
    contact: infosData.data[0].Contact.split("\n"),
    horaires: infosData.data[0].Horaires.split("\n"),
    evenement: infosData.data[0].Evenement ? infosData.data[0].Evenement.split("\n") : null,
    isEventHappening: infosData.data[0].isEventHappening
  }

  return {
    props: { carte, infos }
  }
}

export default Home

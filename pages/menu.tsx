import Carte from '../components/Carte'
import Layout from '../components/Layout'
import { MenuCarte } from '../models/MenuCarte'

interface MenuProps {
  menuCarte: MenuCarte[]
}

const Menu = ({ menuCarte }: MenuProps) => (
  <Layout>
    <main className={`mx-auto mb-8`}>
      {menuCarte.sort((a, b) => a.order - b.order).map((c, idx) => (<Carte key={idx} carte={c} />))}
      <p className='2xl:w-1/3 w-2/3 mx-auto italic'>Nos prix sont en Francs suisses, TVA et autres taxes incluses. </p>
    </main>
  </Layout>
)

export async function getServerSideProps() {
  const [articlesJson] = await Promise.all([
    fetch(`${process.env.DIRECTUS_URL}/items/Article?fields=*,Sub_Category.*,Sub_Category.Category.*,Sub_Category.Category.Carte.*&limit=-1`)
  ])
  const [articlesData] = await Promise.all([articlesJson.json()])

  const menuCarte: MenuCarte[] = articlesData.data.reduce((acc: MenuCarte[], curr: any) => {
    // Is Carte exists in acc ?
    const currCarte = curr.Sub_Category.Category.Carte
    let carteIdx = acc.findIndex(c => c.id === currCarte.id)
    // False : Add Carte in acc
    if (carteIdx < 0) {
      carteIdx = acc.push({
        id: currCarte.id,
        name: currCarte.Name,
        order: currCarte.Order,
        category: []
      })
      carteIdx--
    }

    // Is Category exists in Carte ?
    const currCategory = curr.Sub_Category.Category
    let categoryIdx = acc[carteIdx].category.findIndex(cat => cat.id === currCategory.id)
    // False : Add Category in Carte
    if (categoryIdx < 0) {
      categoryIdx = acc[carteIdx].category.push({
        id: currCategory.id,
        name: currCategory.Name,
        order: currCategory.Order,
        subCat: []
      })
      categoryIdx--
    }

    // Is SubCategory exists in Category ?
    const currSubCat = curr.Sub_Category
    let subCatIdx = acc[carteIdx].category[categoryIdx].subCat.findIndex(sc => sc.id === currSubCat.id)
    // False : Add SubCategory
    if (subCatIdx < 0) {
      subCatIdx = acc[carteIdx].category[categoryIdx].subCat.push({
        id: currSubCat.id,
        name: currSubCat.Name,
        description: currSubCat.Description,
        price: currSubCat.Price,
        order: currSubCat.Order,
        articles: []
      })
      subCatIdx--
    }

    // Add curr Article
    acc[carteIdx].category[categoryIdx].subCat[subCatIdx].articles.push({
      id: curr.id,
      name: curr.Name,
      description: curr.Description,
      price: curr.Price,
      order: curr.Order,
    })

    return acc
  }, [])

  return {
    props: { menuCarte }
  }
}

export default Menu

import { Article } from "./Article";

export interface SubCat{
    id: number,
    name: string,
    description: string,
    price: number,
    articles: Article[],
    order: number
}
import { SubCat } from "./SubCat";

export interface Category {
    id: number,
    name: string,
    subCat:SubCat[],
    order: number
}
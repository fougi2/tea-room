import { Category } from "./Category"

export interface MenuCarte {
    id: number,
    name: string,
    order: number,
    category: Category[]
}
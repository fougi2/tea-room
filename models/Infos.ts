export interface Infos {
    contact: string[],
    horaires: string[],
    evenement: string[] | null,
    isEventHappening: boolean
}
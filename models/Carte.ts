export interface Carte {
    id: number,
    name: string,
    order: number
}
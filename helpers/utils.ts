export const capitalize = (s: string) => {
    return `${s.charAt(0).toLocaleUpperCase()}${s.slice(1)}`
}

export const convertToId = (s: string) => {
    return s.replaceAll(" ", "");
}

export const formatPrice = (price: number) : string=> {
    return price % 1 != 0 ? `${price.toString()}0` : `${price}.- `
}
"use strict";
(() => {
var exports = {};
exports.id = 934;
exports.ids = [934];
exports.modules = {

/***/ 585:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ menu),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./helpers/utils.ts
var utils = __webpack_require__(603);
;// CONCATENATED MODULE: ./components/Article.tsx


const Article = ({ article  })=>{
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "flex place-content-between mt-3 items-center",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                        className: "text-lg",
                        children: article.name
                    }),
                    article.price && /*#__PURE__*/ jsx_runtime_.jsx("p", {
                        children: (0,utils/* formatPrice */.T4)(article.price)
                    })
                ]
            }),
            article.description && /*#__PURE__*/ jsx_runtime_.jsx("p", {
                className: "italic text-sm text-justify",
                children: article.description
            })
        ]
    });
};
/* harmony default export */ const components_Article = (Article);

;// CONCATENATED MODULE: ./components/Carte.tsx



const Carte = ({ carte  })=>{
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "pb-12",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                id: (0,utils/* convertToId */.jK)(carte.name),
                className: "xl:w-2/3 mx-auto text-3xl bg-tea-blue text-center mb-3 py-6",
                children: carte.name
            }),
            carte.category.sort((a, b)=>a.order - b.order
            ).map((cat, idx1)=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "xl:w-1/3 w-2/3 mx-auto px-3",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                            className: "text-3xl mt-6",
                            children: cat.name
                        }),
                        cat.subCat.sort((a, b)=>a.order - b.order
                        ).map((sb, idx2)=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                children: [
                                    sb.name.toLowerCase() !== "null" && /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "flex place-content-between mt-3",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                                className: "text-xl underline",
                                                children: sb.name
                                            }),
                                            sb.price && /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: (0,utils/* formatPrice */.T4)(sb.price)
                                            })
                                        ]
                                    }),
                                    sb.description && /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                        className: "italic text-sm mt-1 text-justify",
                                        children: sb.description
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        children: sb.articles.sort((a, b)=>a.order - b.order
                                        ).map((a, idx)=>/*#__PURE__*/ jsx_runtime_.jsx(components_Article, {
                                                article: a
                                            }, idx)
                                        )
                                    })
                                ]
                            }, idx2)
                        )
                    ]
                }, idx1)
            )
        ]
    });
};
/* harmony default export */ const components_Carte = (Carte);

// EXTERNAL MODULE: ./components/Layout.tsx + 1 modules
var Layout = __webpack_require__(812);
;// CONCATENATED MODULE: ./pages/menu.tsx



const Menu = ({ menuCarte  })=>/*#__PURE__*/ jsx_runtime_.jsx(Layout/* default */.Z, {
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("main", {
            className: `mx-auto mb-8`,
            children: [
                menuCarte.sort((a, b)=>a.order - b.order
                ).map((c, idx)=>/*#__PURE__*/ jsx_runtime_.jsx(components_Carte, {
                        carte: c
                    }, idx)
                ),
                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                    className: "2xl:w-1/3 w-2/3 mx-auto italic",
                    children: "Nos prix sont en Francs suisses, TVA et autres taxes incluses. "
                })
            ]
        })
    })
;
async function getServerSideProps() {
    const [articlesJson] = await Promise.all([
        fetch(`${process.env.DIRECTUS_URL}/items/Article?fields=*,Sub_Category.*,Sub_Category.Category.*,Sub_Category.Category.Carte.*&limit=-1`)
    ]);
    const [articlesData] = await Promise.all([
        articlesJson.json()
    ]);
    const menuCarte = articlesData.data.reduce((acc, curr)=>{
        // Is Carte exists in acc ?
        const currCarte = curr.Sub_Category.Category.Carte;
        let carteIdx = acc.findIndex((c)=>c.id === currCarte.id
        );
        // False : Add Carte in acc
        if (carteIdx < 0) {
            carteIdx = acc.push({
                id: currCarte.id,
                name: currCarte.Name,
                order: currCarte.Order,
                category: []
            });
            carteIdx--;
        }
        // Is Category exists in Carte ?
        const currCategory = curr.Sub_Category.Category;
        let categoryIdx = acc[carteIdx].category.findIndex((cat)=>cat.id === currCategory.id
        );
        // False : Add Category in Carte
        if (categoryIdx < 0) {
            categoryIdx = acc[carteIdx].category.push({
                id: currCategory.id,
                name: currCategory.Name,
                order: currCategory.Order,
                subCat: []
            });
            categoryIdx--;
        }
        // Is SubCategory exists in Category ?
        const currSubCat = curr.Sub_Category;
        let subCatIdx = acc[carteIdx].category[categoryIdx].subCat.findIndex((sc)=>sc.id === currSubCat.id
        );
        // False : Add SubCategory
        if (subCatIdx < 0) {
            subCatIdx = acc[carteIdx].category[categoryIdx].subCat.push({
                id: currSubCat.id,
                name: currSubCat.Name,
                description: currSubCat.Description,
                price: currSubCat.Price,
                order: currSubCat.Order,
                articles: []
            });
            subCatIdx--;
        }
        // Add curr Article
        acc[carteIdx].category[categoryIdx].subCat[subCatIdx].articles.push({
            id: curr.id,
            name: curr.Name,
            description: curr.Description,
            price: curr.Price,
            order: curr.Order
        });
        return acc;
    }, []);
    return {
        props: {
            menuCarte
        }
    };
}
/* harmony default export */ const menu = (Menu);


/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 14:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 20:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 938:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/format-url.js");

/***/ }),

/***/ 565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 52:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 422:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [895,664,381], () => (__webpack_exec__(585)));
module.exports = __webpack_exports__;

})();
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'tea-pink': '#ffc6db',
        'tea-blue': '#7ac5dc',
      },
      backgroundImage: {
        'hero-pattern': "url('/cups.JPG')",
      }
    },
  },
  plugins: [],
}
